<?php

session_start();

if (!isset($_SESSION['username'])) {
    $_SESSION['msg'] = "You must log in first to view this page";
    header("location: login.php");
}

if (isset($_GET['logout'])) {

    session_destroy();
    unset($_SESSION['username']);
    header("location: login.php");
}

$db = mysqli_connect('localhost', 'root', '', 'jacsomedia') or die("could not connect to database");

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>SPEAKER PORTAL</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style_list.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Sortable/1.14.0/Sortable.min.js"></script>
</head>

<body>
    <header class="header">
        <div class="header-row">
            <div class="header-row-left">
                <div class="header-row-left-flex">
                    <img src="img/pills.png" alt="Pills" class="header-row-left-flex-img" />
                    <p class="header-row-left-flex-p"><b>Speaker<br>portal</b></p>
                </div>
            </div>
            <div class="header-row-right">
                <div class="header-row-right-flex">
                    <div class="header-row-right-flex-left-flex-flex">
                        <div class="header-row-right-flex-left-flex">
                            <select name="language" id="language" class="header-row-right-flex-left-flex-select">
                                <option value="english">English</option>
                            </select>
                            <a href="" class="header-row-right-flex-left-flex-a">Contact</a>
                            <a href="" class="header-row-right-flex-left-flex-a">Sitemap</a>
                        </div>
                        <div class="header-row-right-flex-left-flex-button">
                            <button class="header-row-right-flex-left-flex-button-button"><i class="fa fa-folder"></i><b> My Collection</b></button>
                            <button class="header-row-right-flex-left-flex-button-button"><i class="fa fa-close"></i><b> Logout</b></button>
                        </div>
                    </div>
                    <img src="img/logo.png" alt="Logo" class="header-row-right-img" />
                </div>
            </div>
        </div>
    </header>

    <main>
        <div class="main-class">
            <nav class="main-nav">
                <a class="main-nav-icon" href="login.php"><i class="fa fa-home"></i></a>
                <a class="main-nav-button" href="list.php"><b>Mpaf</b></a>
                <a class="main-nav-button" href=""><b>Venous</b></a>
                <a class="main-nav-button" href=""><b>Acs</b></a>
                <a class="main-nav-button" href=""><b>Kivamoxoban studies</b></a>
                <a class="main-nav-button main-nav-button-final" href=""><b>Background information</b></a>
            </nav>
            <?php
            if (isset($_SESSION['success'])) :
            ?>

                <div>
                    <p>
                        <?php
                        //echo ($_SESSION['success']);
                        unset($_SESSION['success'])
                        ?>
                    </p>
                </div>
            <?php endif ?>

            <?php if (isset($_SESSION['username'])) : ?>
                <?php
                $query = "SELECT * FROM list ORDER BY item_order";
                $result = mysqli_query($db, $query);
                ?>
                <div class="main-signin">
                    <div id="item-id-list" class="main-signin-size">
                        <?php while ($rows = mysqli_fetch_assoc($result)) { ?>
                            <div class="row">
                                <table>
                                    <tr>
                                        <td>
                                            <div id="item-ID">
                                                <p class="main-signin-list-item"><?php echo $rows['item_name'] ?></p>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        <?php
                        } ?>
                    </div>
                    <div class="main-signin-button">
                        <button class="main-signin-button" type="submit" name="save_list" onClick="savelist()"><b>Save</b></button>
                    </div>
                    <?php if (isset($_GET["msg"]) && $_GET["msg"] == 'success') : ?>
                        <p class="echo-success"><b><?php echo "Your list has been succesful saved." ?></b></p>
                    <?php endif ?>
                </div>
                <!--
                <p><a href="list.php?logout='1'">Log out</a></p>
                -->
            <?php endif ?>
            <script>
                const dragArea = document.querySelector(".main-signin-size");
                new Sortable(dragArea, {
                    animation: 350
                });
            </script>

            <script>
                function savelist() {
                    var x = document.getElementById("item-id-list").querySelectorAll(".main-signin-list-item");
                    const order = [];
                    for (let i = 0; i < x.length; i++) {
                        order[i] = x[i].innerHTML;
                    }

                    window.location = 'update.php?order=' + order;

                }
            </script>

        </div>
    </main>
    <footer class="footer-class">
        <div class="footer-a">
            <a class="footer-a1" href="">Conditions of use</a>
            <a class="footer-a1" href="">Privacy statement</a>
            <a class="footer-a-last" href="">Imprint</a>
        </div>
        <div class="footer-p">
            <p>This site is intended to provide information to an international audience outside the USA and UK.</p>
        </div>
    </footer>
</body>

</html>