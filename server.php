<?php

session_start();

//initializing variables

$username = "";
$email = "";

$errors = array();

$list = array();

//connect to db

$db = mysqli_connect('localhost', 'root', '', 'jacsomedia') or die("could not connect to database");

//Register users

if (isset($_POST['reg_user'])) {

    $username = mysqli_real_escape_string($db, $_POST['username']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password_1 = mysqli_real_escape_string($db, $_POST['password_1']);
    $password_2 = mysqli_real_escape_string($db, $_POST['password_2']);


    //form validation

    if (empty($username)) {
        array_push($errors, "Username is required");
    }
    if (empty($email)) {
        array_push($errors, "Email is required");
    }
    if (empty($password_1)) {
        array_push($errors, "Password is required");
    }
    if (empty($password_2)) {
        array_push($errors, "Password confirmation is required");
    }
    if ($password_1 != $password_2) {
        array_push($errors, "Passwords do not match");
    }

    // check db for existing user with same username

    $user_check_query = "SELECT * FROM user where username = '$username' OR email = '$email' LIMIT 1";

    $result = mysqli_query($db, $user_check_query);
    $user = mysqli_fetch_assoc($result);

    if ($user) {
        if ($user['username'] === $username) {
            array_push($errors, "Username already exists");
        }
        if ($user['email'] === $email) {
            array_push($errors, "Email already in use");
        }
    }


    //Register if no error

    if (count($errors) == 0) {
        $password = password_hash($password_1, PASSWORD_DEFAULT);
        $query = "INSERT INTO user (username, email, password) VALUES ('$username', '$email', '$password')";
        mysqli_query($db, $query);
        $_SESSION['username'] = $username;
        $_SESSION['success'] = "You are now logged in";

        header('location: list.php');
    }
}

//LOGIN USER

if (isset($_POST['login_user'])) {

    $username = mysqli_real_escape_string($db, $_POST['username']);
    $password = mysqli_real_escape_string($db, $_POST['password']);

    /*if (empty($username)) {
        array_push($errors, "Username is required");
    }
    if (empty($password)) {
        array_push($errors, "Password is required");
    }*/

    if (count($errors) == 0) {
        $query = "SELECT * FROM user WHERE username='$username'";
        $result = mysqli_query($db, $query);

        $user = mysqli_fetch_assoc($result);
        if ($user) {
            if (password_verify($password, $user['password'])) {
                $_SESSION['username'] = $username;
                $_SESSION['success'] = "Logged in successfully";
                header('location:list.php');
            } else {
                //array_push($errors, "Your user name or password was incorrect. Please try it again.");
                header("location:login.php?msg=failed");
            }
        } else {
            //array_push($errors, "Your user name or password was incorrect. Please try it again.");
            header("location:login.php?msg=failed");
        }
    }
}
