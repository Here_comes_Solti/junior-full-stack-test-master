## Task 1:
In **step one**, create a PHP site based on the attached PSD (design/SP-design.psd) file. We are mostly interested in your **CSS and HTML** knowledge. The site should mirror the outlook of the design as much as possible. It does not have to be a responsive result. Optional you can use SCSS or Less if you would like to.
## Task 2:
The **second step** is to measure your **PHP and MySQL** skills. Create a **working login page** for the site you have just created. The login should compare the entered data with the data stored in **database** and run **the verification process the most secure way**. In case the entered username-password combination is **incorrect** (admin, admin123), **a warning message should come up**. Only in case the combination is correct, then users should be navigated to the next page. Users not logged in should not be able to navigate to different menu points. You can write your own business logic, or you can use any framework (Laravel, Yii2, Symfony), the choice is yours.
## Task 3:
**Step three** is about your **JavaScript** skills. You will have to create a **‘grab and take’ list**, where **items can be arranged**, and their arranged position can be **stored in a database** by a single click. The two columns are independent from one another and each item can be placed anywhere. Provide feedback to the user after successful saving. If you would like to use any library (jQuery, React, Angular, VueJs) go for it, or you prefer Vanilla JS it is alright too.

Upload the completed task **to your Github or Gitlab** account with your database dump.
 
During your work, please follow the rules below regarding the HTML and CSS:
* The HTML should be valid per the W3C HTML5 validator.
* Any !important rule in the CSS written by you are prohibited.
* None of the Bootstrap classes allowed to be overwritten.
* Do not qualify the IDs and classes. Some examples about good and bad practices:
  * BAD: button#backButton {…}
  * BAD: treecell.indented {…}
  * GOOD: #backButton {…}
  * GOOD: .treecell-indented {…}
