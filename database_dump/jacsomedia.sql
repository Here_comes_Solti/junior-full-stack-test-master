-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2021. Sze 14. 01:48
-- Kiszolgáló verziója: 10.4.21-MariaDB
-- PHP verzió: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `jacsomedia`
--

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `list`
--

CREATE TABLE `list` (
  `id` int(11) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `list`
--

INSERT INTO `list` (`id`, `item_name`, `item_order`) VALUES
(1, 'SHEEP', 1),
(2, 'CAT', 2),
(3, 'POUND', 3),
(4, 'FOOT', 4),
(5, 'MILK', 5),
(6, 'FROG', 6),
(7, 'COW', 7),
(8, 'DOG', 8),
(9, 'LION', 9),
(10, 'FISH', 10),
(11, 'HEART', 11),
(12, 'SUPER', 12);

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- A tábla adatainak kiíratása `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`) VALUES
(1, 'fa', 'fa@gmail.com', '$2y$10$feR7hEVnrkxTrdR.1UknHeQ7lTQ9mSN6gIgNRBNL7Ewre4B.mUMlm'),
(2, 'haz', 'haz@gmail.com', '$2y$10$yAAGJtApKXf3GVzahcrpf.iQL2ddbuQ0Lu6YGiuQUQU2V6nu3nLRa'),
(3, 'haha', 'haha@gmail.com', '$2y$10$ztX5EFDJe5zHTs3/hPnQ5.Y5eV/W31TXhbvNT3wJYIlPnAoGj/leu');

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `list`
--
ALTER TABLE `list`
  ADD PRIMARY KEY (`id`);

--
-- A tábla indexei `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `list`
--
ALTER TABLE `list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT a táblához `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
