<?php include('server.php') ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>SPEAKER PORTAL</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <header class="header">
        <div class="header-row">
            <div class="header-row-left">
                <div class="header-row-left-flex">
                    <img src="img/pills.png" alt="Pills" class="header-row-left-flex-img" />
                    <p class="header-row-left-flex-p"><b>Speaker<br>portal</b></p>
                </div>
            </div>
            <div class="header-row-right">
                <div class="header-row-right-flex">
                    <div class="header-row-right-flex-left-flex-flex">
                        <div class="header-row-right-flex-left-flex">
                            <select name="language" id="language" class="header-row-right-flex-left-flex-select">
                                <option value="english">English</option>
                            </select>
                            <a href="" class="header-row-right-flex-left-flex-a">Contact</a>
                            <a href="" class="header-row-right-flex-left-flex-a">Sitemap</a>
                        </div>
                        <div class="header-row-right-flex-left-flex-button">
                            <button class="header-row-right-flex-left-flex-button-button"><i class="fa fa-folder"></i><b> My Collection</b></button>
                            <button class="header-row-right-flex-left-flex-button-button"><i class="fa fa-close"></i><b> Logout</b></button>
                        </div>
                    </div>
                    <img src="img/logo.png" alt="Logo" class="header-row-right-img" />
                </div>
            </div>
        </div>
    </header>

    <main>
        <div class="main-class">
            <nav class="main-nav">
                <a class="main-nav-icon" href="login.php"><i class="fa fa-home"></i></a>
                <a class="main-nav-button" href="list.php"><b>Mpaf</b></a>
                <a class="main-nav-button" href=""><b>Venous</b></a>
                <a class="main-nav-button" href=""><b>Acs</b></a>
                <a class="main-nav-button" href=""><b>Kivamoxoban studies</b></a>
                <a class="main-nav-button main-nav-button-final" href=""><b>Background information</b></a>
            </nav>
            <div class="main-signin">
                <form action="login.php" method="post">
                    <div class="main-signin-size">
                        <div class="main-signin-text">
                            <p class="main-signin-text-p">Please Sign in</p>
                        </div>
                        <div class="main-signin-input">
                            <label for="username">User name: </label>
                            <input id="username" type="text" name="username" required>
                        </div>
                        <div class="main-signin-input">
                            <label for="password">Password: </label>
                            <input id="password" type="password" name="password" required>
                        </div>
                        <div class="main-signin-button-div">
                            <button class="main-signin-button" type="submit" name="login_user"><b>Enter</b></button>
                        </div>
                    </div>
                </form>
                <?php if (isset($_GET["msg"]) && $_GET["msg"] == 'failed') : ?>
                    <p class="echo-failed1"><b><?php echo "Your user name or password was incorrect." ?></b></p>
                    <p class="echo-failed2"><b><?php echo "Please try it again." ?></b></p>
                <?php endif ?>
            </div>
        </div>
    </main>
    <footer class="footer-class">
        <div class="footer-a">
            <a class="footer-a1" href="">Conditions of use</a>
            <a class="footer-a1" href="">Privacy statement</a>
            <a class="footer-a-last" href="">Imprint</a>
        </div>
        <div class="footer-p">
            <p>This site is intended to provide information to an international audience outside the USA and UK.</p>
        </div>
    </footer>
</body>

</html>